from django.shortcuts import render
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
import restapi.models
from restapi.models import Authors, NewsStories
import json
# Create your views here.
@csrf_exempt
def Login(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['content-Type'] = 'text/plain'

    if request.method != 'POST':
        http_bad_response.content = "Only POST requests are allowed for this resource"
        return http_bad_response

    username = request.POST.get('username')
    password = request.POST.get('password')

    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        http_response = HttpResponse()
        http_response.status_code = 200
        http_response.reason_phrase = 'OK'
        http_response['Content-Type'] = 'text/plain'
        http_response.content = "Welcome"
        return http_response
    else:
        http_bad_response.status_code = 401
        http_bad_response.reason_phrase = "Unauthorized"
        http_bad_response.content = "No such user matches these credentials"
        return http_bad_response

@csrf_exempt
def Logout(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['content-Type'] = 'text/plain'

    if request.method != 'POST':
        http_bad_response.content = "Only POST requests are allowed for this resource"
        return http_bad_response

    if request.user.is_authenticated:
        http_response = HttpResponse()
        logout(request)
        http_response.status_code = 200
        http_response.reason_phrase = 'OK'
        http_response['Content-Type'] = 'text/plain'
        http_response.content = "Goodbye"
        return http_response
    else:
        http_bad_response.status_code = 400
        http_bad_response.reason_phrase = "Bad Request"
        http_bad_response.content = "No user is currently logged in"
        return http_bad_response

@csrf_exempt
def PostStory(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['content-Type'] = 'text/plain'

    if request.method != 'POST':
        http_bad_response.content = "Only POST requests are allowed for this resource"
        return http_bad_response

    try:
        http_response = HttpResponse()
        data = json.loads(request.body)
        headline = data['headline']
        category = data['category']
        region = data['region']
        details = data['details']
        author = Authors.objects.get(user_id=(User.objects.get(username=request.user).id))
        story = NewsStories(headline=headline, category=category, region=region, details=details, author=author)
        story.save()
        http_response.status_code = 201
        http_response.reason_phrase = 'CREATED'
        return http_response
    except:
        http_bad_response.status_code = 503
        http_bad_response.reason_phrase = "Service Unavailable"
        http_bad_response.content = "Unable to post story"
        return http_bad_response


def GetStories(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['content-Type'] = 'text/plain'

    if request.method != 'GET':
        http_bad_response.content = "Only GET requests are allowed for this resource"
        return http_bad_response

    try:
        data = json.loads(request.body)
        category = data['story_cat']
        region = data['story_region']
        date = data['story_date']
        allstories = NewsStories.objects.all().values('id', 'headline', 'category', 'region', 'author', 'date', 'details')

        if category != '*':
            allstories = allstories.filter(category=category)
        if region != '*':
            allstories=allstories.filter(region=region)
        if date != '*':
            allstories=allstories.filter(date__geq=date)


        stories = []
        for item in allstories:
            author = Authors.objects.get(user_id=item['author']).name
            story = {'key': item['id'], 'headline': item['headline'], 'story_cat': item['category'], 'story_region': item['region'], 'author': author, 'story_date': str(item['date']), 'story_details': item['details']}
            stories.append(story)

        print(stories)
        payload = {'stories': stories}
        http_response = HttpResponse(json.dumps(payload))
        http_response['Content-Type'] = 'application/json'
        http_response.status_code = 200
        http_response.reason_phrase = 'OK'
        return http_response
    except:
        http_bad_response.status_code = 404
        http_bad_response.reason_phrase = 'Not Found'
        http_bad_response.content = "No stories could be found"
        return http_bad_response

@csrf_exempt
def DeleteStories(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['content-Type'] = 'text/plain'

    if request.method != 'POST':
        http_bad_response.content = "Only POST requests are allowed for this resource"
        return http_bad_response

    try:
        data = json.loads(request.body)
        key = data['story_key']
        story = NewsStories.objects.get(id=key)
        story.delete()
        http_response = HttpResponse()
        http_response.status_code = 201
        http_response.reason_phrase = 'CREATED'
        return http_response
    except:
        http_bad_response.status_code = 503
        http_bad_response.reason_phrase = 'Service Unavailable'
        http_bad_response.content = "Unable to locate story"
        return http_bad_response
