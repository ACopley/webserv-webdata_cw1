from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Authors(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length=25)
#    username = models.CharField(max_length=20)
#    password = models.CharField(max_length=20)


Categories = [{'pol', 'politics'}, {'art', 'art news'}, {'tech', 'technology news'}, {'trivia', 'trivial news'}]
Regions = [{'uk', 'UK'}, {'eu', 'European News'}, {'w', 'World News'}]

class NewsStories(models.Model):
    headline = models.CharField(max_length=64)
    category = models.CharField(max_length=30, choices=Categories)
    region = models.CharField(max_length=30, choices=Regions)
    author = models.ForeignKey(Authors, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)
    details = models.CharField(max_length=512)
