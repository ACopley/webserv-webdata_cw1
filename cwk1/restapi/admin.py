from django.contrib import admin
from .models import NewsStories, Authors

# Register your models here.
admin.site.register(Authors)
admin.site.register(NewsStories)
